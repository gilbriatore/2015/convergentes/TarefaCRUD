package br.edu.up;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;



// Will map the resource to the URL Tarefas
@Path("/tarefas")
public class TarefasResource {

  // Allows to insert contextual objects into the class,
  // e.g. ServletContext, Request, Response, UriInfo
  @Context
  UriInfo uriInfo;
  
  @Context
  Request request;

  // Return the list of Tarefas to the user in the browser
  @GET
  @Produces(MediaType.TEXT_XML)
  public List<Tarefa> getTarefasBrowser() {
    List<Tarefa> Tarefas = new ArrayList<Tarefa>();
    Tarefas.addAll(DAO.instance.getModel().values());
    return Tarefas;
  }

  // Return the list of Tarefas for applications
  @GET
  @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
  public List<Tarefa> getTarefas() {
    List<Tarefa> Tarefas = new ArrayList<Tarefa>();
    Tarefas.addAll(DAO.instance.getModel().values());
    return Tarefas;
  }

  // retuns the number of Tarefas
  // Use http://localhost:8080/com.vogella.jersey.Tarefa/rest/Tarefas/count
  // to get the total number of records
  @GET
  @Path("count")
  @Produces(MediaType.TEXT_PLAIN)
  public String getCount() {
    int count = DAO.instance.getModel().size();
    return String.valueOf(count);
  }

  @POST
  @Produces(MediaType.TEXT_HTML)
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  public void newTarefa(@FormParam("id") String id,
      @FormParam("titulo") String titulo,
      @FormParam("descricao") String descricao,
      @Context HttpServletResponse servletResponse) throws IOException {
    Tarefa Tarefa = new Tarefa(id, titulo);
    if (descricao != null) {
      Tarefa.setDescricao(descricao);
    }
    DAO.instance.getModel().put(id, Tarefa);

    servletResponse.sendRedirect("../sucesso.html");
  }

  // Defines that the next path parameter after Tarefas is
  // treated as a parameter and passed to the TarefaResources
  // Allows to type http://localhost:8080/com.vogella.jersey.Tarefa/rest/Tarefas/1
  // 1 will be treaded as parameter Tarefa and passed to TarefaResource
  @Path("{Tarefa}")
  public TarefaResource getTarefa(@PathParam("Tarefa") String id) {
    return new TarefaResource(uriInfo, request, id);
  }

} 