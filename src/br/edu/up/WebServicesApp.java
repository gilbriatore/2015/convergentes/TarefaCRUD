package br.edu.up;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
  
@ApplicationPath("rest")
public class WebServicesApp  extends ResourceConfig {
    public WebServicesApp() {
        packages("br.edu.up");
    }
}