package br.edu.up;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Tarefa {
	
	private String id;
	private String titulo;
	private String descricao;
	
	public Tarefa() {
	}

	public Tarefa(String id, String titulo) {
		this.id = id;
		this.titulo = titulo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}