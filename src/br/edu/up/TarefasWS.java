package br.edu.up;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/tarefa")
public class TarefasWS {

  @GET
  @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public Tarefa get() {
    Tarefa tarefa = new Tarefa();
    tarefa.setTitulo("Tarefa 1");
    tarefa.setDescricao("Esta � minha primeira tarefa.");
    return tarefa;
  }
} 