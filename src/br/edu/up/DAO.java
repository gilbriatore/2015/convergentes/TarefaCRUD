package br.edu.up;

import java.util.HashMap;
import java.util.Map;

public enum DAO {
  instance;
  
  private Map<String, Tarefa> contentProvider = new HashMap<>();
  
  private DAO() {
    
    Tarefa tarefa = new Tarefa("1", "Learn REST");
    tarefa.setDescricao("Read http://www.vogella.com/tutorials/REST/article.html");
    contentProvider.put("1", tarefa);
    tarefa = new Tarefa("2", "Do something");
    tarefa.setDescricao("Read complete http://www.vogella.com");
    contentProvider.put("2", tarefa);
    
  }
  public Map<String, Tarefa> getModel(){
    return contentProvider;
  }
  
} 