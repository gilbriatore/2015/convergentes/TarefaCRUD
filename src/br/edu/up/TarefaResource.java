package br.edu.up;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;

public class TarefaResource {
	
	@Context
	UriInfo uriInfo;
	@Context
	Request request;
	String id;

	public TarefaResource(UriInfo uriInfo, Request request, String id) {
		this.uriInfo = uriInfo;
		this.request = request;
		this.id = id;
	}

	// Application integration
	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Tarefa getTarefa() {
		Tarefa Tarefa = DAO.instance.getModel().get(id);
		if (Tarefa == null)
			throw new RuntimeException("Get: Tarefa with " + id + " not found");
		return Tarefa;
	}

	// for the browser
	@GET
	@Produces(MediaType.TEXT_XML)
	public Tarefa getTarefaHTML() {
		Tarefa Tarefa = DAO.instance.getModel().get(id);
		if (Tarefa == null)
			throw new RuntimeException("Get: Tarefa with " + id + " not found");
		return Tarefa;
	}

	@PUT
	@Consumes(MediaType.APPLICATION_XML)
	public Response putTarefa(JAXBElement<Tarefa> Tarefa) {
		Tarefa c = Tarefa.getValue();
		return putAndGetResponse(c);
	}

	@DELETE
	public void deleteTarefa() {
		Tarefa c = DAO.instance.getModel().remove(id);
		if (c == null)
			throw new RuntimeException("Delete: Tarefa with " + id + " not found");
	}

	private Response putAndGetResponse(Tarefa Tarefa) {
		Response res;
		if (DAO.instance.getModel().containsKey(Tarefa.getId())) {
			res = Response.noContent().build();
		} else {
			res = Response.created(uriInfo.getAbsolutePath()).build();
		}
		DAO.instance.getModel().put(Tarefa.getId(), Tarefa);
		return res;
	}

}