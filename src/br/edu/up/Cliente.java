package br.edu.up;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

public class Cliente {

  public static void main(String[] args) {

    Client cliente = ClientBuilder.newClient();
    WebTarget ws = cliente.target(getBaseURI());

    String xml =ws.path("rest").path("tarefa").request()
        .accept(MediaType.APPLICATION_XML).get(String.class);
    String json = ws.path("rest").path("tarefa").request()
        .accept(MediaType.APPLICATION_JSON).get(String.class);
    
    System.out.println(xml);
    System.out.println(json);
  }

  private static URI getBaseURI() {
    return UriBuilder.fromUri("http://localhost:8080/TarefaCRUD").build();
  }
} 