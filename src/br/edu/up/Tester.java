package br.edu.up;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;


public class Tester {
  public static void main(String[] args) {
	  
	  //http://localhost:8080/TarefaCRUD/rest/tarefas
	  //http://localhost:8080/TarefaCRUD/rest/tarefas/count
	  //http://localhost:8080/TarefaCRUD/rest/tarefas/1

    Client cliente = ClientBuilder.newClient();
    WebTarget ws = cliente.target(getBaseURI());

    //Cria uma tarefa
    Tarefa tarefa = new Tarefa("1", "Blabla");
    Response response = ws.path("rest")
    		.path("tarefas")
    		.path(tarefa.getId())
    		.request(MediaType.APPLICATION_XML)
    		.put(Entity.entity(tarefa,MediaType.APPLICATION_XML),Response.class);

    //O retorno 201 informa que a tarefa foi criada.
    System.out.println(response.getStatus());

    //Lista de tarefas
    String lista = ws.path("rest").
    		path("tarefas")
    		.request()
    		.accept(MediaType.TEXT_XML)
    		.get(String.class);
    
    System.out.println(lista);

    //Lista de tarefas em JSON
    String json = ws.path("rest")
    		.path("tarefas")
    		.request()
    		.accept(MediaType.APPLICATION_JSON)
    		.get(String.class);
    System.out.println(json);

    //Lista de tarefas em XML
    String xml = ws.path("rest")
    		.path("tarefas")
    		.request()
    		.accept(MediaType.APPLICATION_XML)
    		.get(String.class);
    System.out.println(xml);

    //Pega a tarefa 1
    Response tarefa1 = ws.path("rest")
    		.path("tarefas/1")
    		.request()
    		.accept(MediaType.APPLICATION_XML)
    		.get();

    //Apaga a tarefa 1
    Response confirma = ws.path("rest")
    		.path("tarefas/1")
    		.request()
    		.delete();
    System.out.println(confirma); 

    //Lista as tarefas tarefa 1 apagada.
    String tarefas = ws.path("rest")
    		.path("tarefas")
    		.request()
    		.accept(MediaType.APPLICATION_XML)
    		.get(String.class);
    System.out.println(tarefas);

    //Cria a tarefa 4.
    Form form = new Form();
    form.param("id", "4");
    form.param("summary","Demonstration of the client lib for forms");
    response = ws.path("rest").path("tarefas").request().post(Entity.entity(form,MediaType.APPLICATION_FORM_URLENCODED),Response.class);
    System.out.println("Form response " + response.getStatus());

    //Lista todas as tarefas a tarefa 4 deve ter sido criada.
    tarefas = ws.path("rest")
    		.path("tarefas")
    		.request()
    		.accept(MediaType.APPLICATION_XML)
    		.get(String.class);
    System.out.println(tarefas);

  }

  private static URI getBaseURI() {
    return UriBuilder.fromUri("http://localhost:8080/TarefaCRUD").build();
  }
}